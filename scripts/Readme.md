This folder will contain all scripts which will be used to get data from PubMed.

The following are the scripts available

# Journal List with Keyword Cancer

This script fetches a list of NLM journals from PubMed with the keyword Cancer and restricted to the journals which are in the NCBI list. The following fields are extracted from the data:

1.  NLMID
2.  Journal Title
3.  Journal Abbreviation
4.  Journal URL
5.  Indexed in PubMed or not
6.  Journal publication type
7.  Country of origin
8.  Primary language

This data is saved in the form of RDS file in the data folder. The file name is jrnl_list_cancer_keyword.rds

# Journal List with Keyword Oncology

This is similar to the above journal with the difference that the keyword used is Oncology and that file save is called jrnl_list_oncology_keyword.rds. Field names are same.

# Journal List with Keyword Sarcoma

This is similar to the above journal with the difference that the keyword used is Sarcoma and that file save is called jrnl_list_sarcoma_keyword.rds. Field names are same.

# Journal List with Subject Terms

This is a list generated from various subject terms as noted in the code file. Same file structure as above. File is saved in jrnl_list_st.rds. Field names are also same.
