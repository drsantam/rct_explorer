library(tidyverse) # General data wrangling and plotting
library(jsonlite) # Working with JSON data returned by the script. 
library(httr) # Package to get data from API calls
library(xml2) # Package to work with XML data

setwd(getwd())
# Searching for journals in NLM Catalog which have the keyword cancer (expanded by MeSH) and are indexed in NLM

# Base URL for Pubmed search
u = "https://eutils.ncbi.nlm.nih.gov/entrez/eutils/esearch.fcgi" 
# NLM catalog has the NLM journal IDs
db = "db=nlmcatalog"
# Term is the search term to be used. 
term = "term=cancer+AND+ncbijournals[filter]"
# Paste the API key with the call
api_key=paste("api_key=",Sys.getenv("api_key"),sep="")
# Set the mode in which the result is returned
r = "retmode=json"
# Set the max number of IDs to be returned from entrez search
m = "retmax=1000"
#Use the history server to facilitate future queries.
h = "usehistory=y"
join <- paste(db,term,api_key,r,m,h,sep="&") # make the joined url with ampersand seperator
qurl = paste(u,join,sep="?") # Make the full url with question mark separate

# Use httr POST to get the data. We use POST instead of GET in view of the large resullt size. 
call <- POST(url = qurl)

# Get the content from the POST call
data <- content(call,"text") 

# Convert to a JSON object using JSONLite library

data <- fromJSON(data) 

# Store the Webenv and QueryKey from the data object into a vector for the fetch query
webenv <- data$esearchresult$webenv
querykey <- data$esearchresult$querykey


# Fetch Journal names from the NLM catalog

u = "https://eutils.ncbi.nlm.nih.gov/entrez/eutils/efetch.fcgi"
db = "db=nlmcatalog"
env = paste("webenv",webenv,sep="=")
q = paste("query_key",querykey,sep="=")
r = "retmode=xml"
# We use the NLM history server to get IDs we have saved in the history server in the previous step
join = paste(db,q,env,api_key,h,r,m,sep="&")

qurl = paste(u,join,sep="?")

dt <- POST(url=qurl)

# The returned object is an XML object which we expressly read with read_xml to convert to a R object

data <- read_xml(dt)

# Convert the xml to a list and then to a tibble to allow XML data extraction using tidyverse
data %>% as_list(.) %>% as_tibble(.) -> data

# Using Hoist we get a data frame that has the following components:
# 1. NLMID
# 2. Title
# 3. Language
# 4. Abbreviation
# 5. URL
# Note that these will generate a dataframe with lists which we will extract individually

data %>% 
  hoist(NLMCatalogRecordSet,
        nlmid = "NlmUniqueID",
        title = "TitleMain",
        lang = "Language",
        abbr = "MedlineTA",
        url = "ELocationList",
        index = "IndexingSourceList",
        pub_type = "PublicationTypeList",
        pub_info = "PublicationInfo"
        ) %>% 
  mutate(id = row_number()) -> data_wide

# Extract the NLM ID from the list
data_wide %>% 
  select(id,nlmid) %>% 
  unnest_longer(nlmid) -> df1

# Extract the title and merge with previous dataframe

data_wide %>% select(id,title)  %>% 
  unnest_longer(title) %>% 
  filter(title_id == "Title") %>% 
  unnest_longer(title) %>% 
  select(id,title) %>% 
  left_join(df1,.,by="id") -> df1

# Extract language and merge with previous data frame. As languages can be more than one we use distinc to ensure only the first one gets in the dataframe.
data_wide %>% 
  select(id,lang) %>% 
  unnest_longer(lang) %>% 
  distinct(id,.keep_all = T) %>% 
  left_join(df1,.,by="id") -> df1

# Extract Journal abbreviation and merge with previous data frame

data_wide %>% 
  select(id,abbr) %>% 
  unnest_longer(abbr) %>% 
  left_join(df1,.,by="id") -> df1

# Extract URL and merge with previous data frame. As journal can have multiple URLs we keep just one
data_wide %>% 
  select(id,url) %>% 
  unnest_longer(url) %>% 
  filter(url_id == "ELocation") %>% 
  select(id,url) %>% 
  unnest_longer(url) %>% 
  filter(url_id == "ELocationID") %>% 
  select(id,url) %>% 
  unnest_longer(url) %>% 
  distinct(id,.keep_all=T) %>% 
  left_join(df1,.,by="id") -> df1

# Extract publication type and merge with previous record

data_wide %>% 
  select(id,pub_type) %>% 
  unnest_longer(pub_type) %>% 
  filter(!is.na(pub_type_id)) %>% 
  select(id,pub_type) %>% 
  unnest_longer(pub_type) %>% 
  distinct(id,.keep_all=T) %>% 
  left_join(df1,.,by="id") -> df1

# Check the indexing source and merge with previous record

data_wide %>% 
  select(id,index) %>% 
  unnest_longer(index) %>% 
  select(id,index) %>% 
  unnest_longer(index) %>% 
  filter(index_id == "IndexingSourceName") %>% 
  select(id,index) %>% 
  unnest_longer(index) %>% 
  group_by(id,index) %>% 
  summarise(n = n()) %>% 
  pivot_wider(id_cols = id,names_from = index, values_from = n) %>% 
  filter(PubMed == 1) %>% 
  mutate(index = "Pubmed") %>% 
  select(id,index) %>% 
  left_join(df1,.,by="id") -> df1

# Get the country from the data and merge with previous record
data_wide %>% 
  select(id,pub_info) %>% 
  hoist(pub_info,country = "Country") %>% 
  select(id,country) %>% 
  unnest_longer(country) %>% 
  left_join(df1,.,by="id") -> df1

saveRDS(df1,"data/Jrnl_list_cancer_keyword.rds")
